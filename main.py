"""
Filename: main.py
related files: methods.py
Programmer: William Doyle
Date: December 1st 2018
"""

import methods
from methods import player, Game
import pygame
import time
pygame.init()

FINAL_WINDOW_WIDTH = 700
FINAL_WINDOW_LENGTH = 700
FINAL_SIZE = [FINAL_WINDOW_WIDTH, FINAL_WINDOW_LENGTH]

def main():
	player1 = player()
	print(player1.y)
	player1.move_up()
	print(player1.y)
	print(player1.location())
	game = Game(FINAL_SIZE)
	c = (0,255,0)
	game.changeScreenColor(c)
	game.drawPlayer(player1.location())
	player1.playerControls( game)



if __name__ == '__main__':
	main()
